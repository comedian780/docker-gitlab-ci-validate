# docker-gitlab-ci-validate

This repository enables [gitlab-ci-validate](https://github.com/Code0x58/gitlab-ci-validate) to be run inside a Docker container

## Usage

You can run tests against the gitlab.com endpoint:  
If no parameter is given the container will look for a file called `.gitlab-ci.yml`
```sh
docker run -i --rm \
-v ${PWD}/.gitlab-ci.yml:/yaml/.gitlab-ci.yml \
registry.gitlab.com/comedian780/docker-gitlab-ci-validate
```

You can run tests against a self hosted Gitlab instance with custom filenames:  
Set the credentials and URL via the `GITLAB_HOST` environment variable  
```sh
docker run -i --rm \
-e GITLAB_HOST=https://GITLAB_USER:GITLAB_PW@your.gitlab.server \
-v ${PWD}:/yaml \
-v /additional/folder/.additional.yml:/yaml/.additional.yml \
registry.gitlab.com/comedian780/docker-gitlab-ci-validate custom.yml .files.yml .additional.yml
```

You can also test all YAML files inside a directory (this also includes YAML files in subdirectories):
```sh
find $PWD -type f -iname \*.yml | xargs docker run -i --rm -v ${PWD}:${PWD} \
registry.gitlab.com/comedian780/docker-gitlab-ci-validate
```
